﻿using System;
using System.Linq;

namespace TaskChainingAndContinuationLibrary
{
    public class TaskChainer
    {
        readonly Random randInt;
        public TaskChainer()
        {
            randInt = new Random();
        }

        public int[] CreateArrayOfTenRandomNumbers(int[] arrayOfTen)
        {
            CheckConditions(arrayOfTen);

            for (int i = 0; i < 10; i++)
            {
                arrayOfTen[i] = this.randInt.Next(int.MinValue, int.MaxValue);
            }
            return arrayOfTen;
        }

        public int[] MultiplyArrayByRandomNumber(int[] arrayOfTen)
        {
            CheckConditions(arrayOfTen);

            int randomMultiplier = new Random().Next();

            for (int i = 0; i < 10; i++)
            {
                arrayOfTen[i] = arrayOfTen[i] * randomMultiplier;
            }
            return arrayOfTen;
        }

        public int[] SortArrayByAscending(int[] arrayOfTen)
        {
            CheckConditions(arrayOfTen);
            Array.Sort(arrayOfTen);
            return arrayOfTen;
        }

        public double CalculateAverageValueOfArray(int[] arrayOfTen)
        {
            CheckConditions(arrayOfTen);

            return arrayOfTen.Average();
        }

        public bool CheckConditions(int[] arrayOfTen)
        {
            if (arrayOfTen is null)
            {
                throw new ArgumentNullException(nameof(arrayOfTen), "Array should not be null");
            }
            if (arrayOfTen.Length == 0)
            {
                throw new ArgumentException("Array should have numbers inside", nameof(arrayOfTen));
            }

            return true;
        }

    }
}
