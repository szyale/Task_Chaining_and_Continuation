﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TaskChainingAndContinuationLibrary;

namespace TaskChainingAndContinuation
{
    public class Program
    {
        static void Main()
        {
            var taskChainer = new TaskChainer();
            var arrayOfTen = new int[10];
            var delay = 1500;

            Task <int[]> createTask = Task.Run(() =>
                {
                Task.Delay(delay);
                    return taskChainer.CreateArrayOfTenRandomNumbers(arrayOfTen);
                }
            );

            Task <int[]> multiplyTask = createTask.ContinueWith(task => taskChainer.MultiplyArrayByRandomNumber(createTask.Result));
            Task <int[]> sortTask = multiplyTask.ContinueWith(task => taskChainer.SortArrayByAscending(multiplyTask.Result));
            Task <double> calcTask = sortTask.ContinueWith(task => taskChainer.CalculateAverageValueOfArray(sortTask.Result));


            Console.WriteLine("Array contains following values: ");
            foreach (var item in createTask.Result)
            {
                Console.Write(item + "\t\n");
            }
            Console.WriteLine("Average value of the array {0}", calcTask.Result);
        }
    }
}
