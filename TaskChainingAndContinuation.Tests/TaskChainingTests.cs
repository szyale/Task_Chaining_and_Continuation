using NUnit.Framework;
using System;
using TaskChainingAndContinuationLibrary;

namespace TaskChainingAndContinuation.Tests
{
    public class TaskChainingTests
    {

        private TaskChainer taskChainer;
        private int[] array = new int[10];

        [SetUp]
        public void Setup()
        {
            taskChainer = new TaskChainer();
        }

        [Test]
        public void CreatesArrayOfTenOkResult()
        {
            this.array = taskChainer.CreateArrayOfTenRandomNumbers(this.array);
            Assert.AreEqual(10, this.array.Length);
        }

        [Test]
        public void CreatesArrayOfTenNOKResultNull()
        {
            Assert.Throws<ArgumentNullException>(() => taskChainer.CreateArrayOfTenRandomNumbers(null));
        }

        [Test]
        public void CreatesArrayOfTenNOKResultLength()
        {
            var arr = new int[0];
            Assert.Throws<ArgumentException>(() => taskChainer.CreateArrayOfTenRandomNumbers(arr));
        }

        [Test]
        public void MultiplyArrayOkResult()
        {
            this.array = taskChainer.MultiplyArrayByRandomNumber(this.array);
            Assert.IsTrue(this.array.Length == 10);
        }

        [Test]
        public void MultiplyArrayNOKResultNull()
        {
            Assert.Throws<ArgumentNullException>(() => taskChainer.MultiplyArrayByRandomNumber(null));
        }

        [Test]
        public void MultiplyArrayNOKResultLength()
        {
            var arr = new int[0];
            Assert.Throws<ArgumentException>(() => taskChainer.MultiplyArrayByRandomNumber(arr));
        }

        [Test]
        public void SortArrayOkResult()
        {
            this.array = taskChainer.SortArrayByAscending(this.array);
            Assert.IsTrue(this.array[1] < this.array[2]);
        }

        [Test]
        public void SortArrayNOKResultNull()
        {
            Assert.Throws<ArgumentNullException>(() => taskChainer.SortArrayByAscending(null));
        }

        [Test]
        public void SortArrayNOKResultLenght()
        {
            var arr = new int[0];
            Assert.Throws<ArgumentException>(() => taskChainer.SortArrayByAscending(arr));
        }

        [Test]
        public void CalculateAverageOkResult()
        {
            var result = taskChainer.CalculateAverageValueOfArray(this.array);
            Assert.IsTrue(result.GetType().IsValueType);
        }

        [Test]
        public void CalculateAverageNOKResultNull()
        {
            Assert.Throws<ArgumentNullException>(() => taskChainer.CalculateAverageValueOfArray(null));
        }

        [Test]
        public void CalculateAverageNOKResultLength()
        {
            var arr = new int[0];
            Assert.Throws<ArgumentException>(() => taskChainer.CalculateAverageValueOfArray(arr));
        }

        [Test]
        public void CheckCoditionalsOkResult()
        {
            var result = taskChainer.CheckConditions(this.array);
            Assert.IsTrue(result);
        }

        [Test]
        public void CheckCoditionalsNOkResultNull()
        {
            Assert.Throws<ArgumentNullException>(() => taskChainer.CheckConditions(null));
        }

        [Test]
        public void CheckCoditionalsNOkResultLength()
        {
            var arr = new int[0];
            Assert.Throws<ArgumentException>(() => taskChainer.CheckConditions(arr));
        }



    }
}